(use-package compile-angel
  ;;:ensure t
  :demand t
  :config
  ;; Set `compile-angel-verbose` to nil to suppress output from compile-angel.
  ;; Drawback: The minibuffer will not display compile-angel's actions.
  (setq compile-angel-verbose t)

  (compile-angel-on-load-mode)
  (add-hook 'emacs-lisp-mode-hook #'compile-angel-on-save-local-mode))

;; Keybindings
(global-unset-key (kbd "C-x f"))
(global-set-key (kbd "C-x f") (lambda () (interactive) (let '(default-directory "~/") (call-interactively 'find-file))))

;; Enable line wrap on word boundaries
(global-visual-line-mode t)

;; Miscelaneous (i'm yet to discover a better place to put these ones)
(setq-default c-basic-offset 4)
(setq read-file-name-completion-ignore-case t
      read-buffer-completion-ignore-case t
      completion-ignore-case t
      completion-category-defaults nil
      completion-category-overrides nil
      completion-styles '(basic)
      ;; To prevent Emacs from saving customization information to a custom file
      custom-file null-device)

;; Auto-revert in Emacs is a feature that automatically updates the
;; contents of a buffer to reflect changes made to the underlying file
;; on disk.
(add-hook 'after-init-hook #'global-auto-revert-mode)

;; recentf is an Emacs package that maintains a list of recently
;; accessed files, making it easier to reopen files you have worked on
;; recently.
(add-hook 'after-init-hook #'recentf-mode)
;; Disable unnecessary messages on startup
(setq recentf-auto-cleanup 'never)  ;; Prevent automatic cleanup messages
(with-temp-message ""  ;; Suppress messages during recentf loading
  (require 'recentf)
  (recentf-mode 1))

;; savehist is an Emacs feature that preserves the minibuffer history between
;; sessions. It saves the history of inputs in the minibuffer, such as commands,
;; search strings, and other prompts, to a file. This allows users to retain
;; their minibuffer history across Emacs restarts.
(add-hook 'after-init-hook #'savehist-mode)

;; save-place-mode enables Emacs to remember the last location within a file
;; upon reopening. This feature is particularly beneficial for resuming work at
;; the precise point where you previously left off.
(add-hook 'after-init-hook #'save-place-mode)

;; Display relative line numbers in every buffer
(global-display-line-numbers-mode 1)
(setq display-line-numbers-type 'relative)

;; Set font size
(set-face-attribute 'default nil :height 120)

(use-package gruvbox-theme
  :config
  (load-theme 'gruvbox-dark-hard t))

(use-package vterm
  :ensure t
  :defer t
  :commands vterm
  :config
  ;; Speed up vterm
  (setq vterm-timer-delay 0.01))

(use-package vertico
  ;; (Note: It is recommended to also enable the savehist package.)
  :ensure t
  :defer t
  :commands vertico-mode
  :hook (after-init . vertico-mode))

(use-package orderless
  ;; Vertico leverages Orderless' flexible matching capabilities, allowing users
  ;; to input multiple patterns separated by spaces, which Orderless then
  ;; matches in any order against the candidates.
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-defaults nil)
  (completion-category-overrides '((file (styles partial-completion)))))

(use-package marginalia
  ;; Marginalia allows Embark to offer you preconfigured actions in more contexts.
  ;; In addition to that, Marginalia also enhances Vertico by adding rich
  ;; annotations to the completion candidates displayed in Vertico's interface.
  :ensure t
  :defer t
  :commands (marginalia-mode marginalia-cycle)
  :hook (after-init . marginalia-mode))

(use-package embark
  ;; Embark is an Emacs package that acts like a context menu, allowing
  ;; users to perform context-sensitive actions on selected items
  ;; directly from the completion interface.
  :ensure t
  :defer t
  :commands (embark-act
             embark-dwim
             embark-export
             embark-collect
             embark-bindings
             embark-prefix-help-command)
  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'

  :init
  (setq prefix-help-command #'embark-prefix-help-command)

  :config
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))

(use-package mood-line
  :config
  (mood-line-mode))

;; evil-want-keybinding must be declared before Evil and Evil Collection
(setq evil-want-keybinding nil)

(use-package evil
  :ensure t
  :init
  (setq evil-undo-system 'undo-fu)
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-respect-visual-line-mode t)
  :custom
  (evil-want-Y-yank-to-eol t)
  :config
  (evil-select-search-module 'evil-search-module 'evil-search)
  (evil-mode 1))

(use-package evil-collection
  :after evil
  :ensure t
  :config
  (evil-collection-init))

(use-package undo-fu
  :ensure t
  :commands (undo-fu-only-undo
             undo-fu-only-redo
             undo-fu-only-redo-all
             undo-fu-disable-checkpoint)
  :custom
  ;; 3 times the default values
  (undo-limit (* 3 160000))
  (undo-strong-limit (* 3 240000)))

(use-package undo-fu-session
  :ensure t
  :config
  (undo-fu-session-global-mode))

(use-package org
  :ensure nil
  :custom
  (org-priority-lowest 68)
  (org-startup-folded t)
  (org-clock-sound "~/Músicas/mixkit-arcade-score-interface-217.wav")
  ;; Latex dispaly configuration
  (org-format-latex-options '(:foreground default :background default :scale 2.2 :html-foreground "Black" :html-background "Transparent" :html-scale 1.0 :matchers
                                          ("begin" "$1" "$" "$$" "\\(" "\\[")))
  (org-startup-with-latex-preview t)
  ;; Org to HTML configuration
  (org-html-doctype "html5")
  (org-html-head "<link rel=\"stylesheet\" href=\"style.css\" />")
  (org-html-head-extra "")
  (org-html-head-include-default-style nil)
  (org-html-head-include-scripts nil)
  (org-html-preamble nil)
  (org-html-postamble nil)
  (org-html-use-infojs nil)
  (org-export-with-title nil)
  (org-html-headline-levels 6)
  (org-export-with-toc nil)
  (org-export-with-section-numbers nil)
  (org-publish-project-alist
   '(("mofu.blog"
      :base-extension "org"
      :base-directory "~/Projetos/Grand/mofu.blog/"
      :publishing-directory "~/Projetos/Grand/mofu.blog/"
      :recursive t
      :publishing-function org-html-publish-to-html
      :headline-levels 6
      )))
  :config
  (defun org-html--build-meta-info (info)
    "Return meta tags for exported document.
INFO is a plist used as a communication channel."
    (let* ((title (org-html-plain-text
                   (org-element-interpret-data (plist-get info :title)) info))
           ;; Set title to an invisible character instead of leaving it
           ;; empty, which is invalid.
           (title (if (org-string-nw-p title) title "Möfu"))
           (charset (or (and org-html-coding-system
                             (symbol-name
                              (coding-system-get org-html-coding-system
                                                 'mime-charset)))
                        "iso-8859-1")))
      (concat
       (when (plist-get info :time-stamp-file)
         (format-time-string
          (concat "<!-- "
                  (plist-get info :html-metadata-timestamp-format)
                  " -->\n")))

       (if (org-html-html5-p info)
           (org-html--build-meta-entry "charset" charset)
         (org-html--build-meta-entry "http-equiv" "Content-Type"
                                     (concat "text/html;charset=" charset)))

       (let ((viewport-options
              (cl-remove-if-not (lambda (cell) (org-string-nw-p (cadr cell)))
                                (plist-get info :html-viewport))))
         (if viewport-options
             (org-html--build-meta-entry "name" "viewport"
                                         (mapconcat
                                          (lambda (elm)
                                            (format "%s=%s" (car elm) (cadr elm)))
                                          viewport-options ", "))))

                                        ;(format "<title>%s</title>\n" title)
       "<title>Möfu</title>\n"

       (mapconcat
        (lambda (args) (apply #'org-html--build-meta-entry args))
        (delq nil (if (functionp org-html-meta-tags)
                      (funcall org-html-meta-tags info)

                    org-html-meta-tags))
        ""))))

  (defun org-html--build-head (info)
    "Return information for the <head>..</head> of the HTML output.
INFO is a plist used as a communication channel."
    (org-element-normalize-string
     (concat
      (when (plist-get info :html-head-include-default-style)
        (org-element-normalize-string org-html-style-default))
      (org-element-normalize-string (plist-get info :html-head))
      (org-element-normalize-string (plist-get info :html-head-extra))
      (when (and (plist-get info :html-htmlized-css-url)
                 (eq org-html-htmlize-output-type 'css))
        (org-html-close-tag "link"
                            (format "rel=\"stylesheet\" href=\"%s\" type=\"text/css\""
                                    (plist-get info :html-htmlized-css-url))
                            info))
      (when (plist-get info :html-head-include-scripts) org-html-scripts))))

  (advice-add 'org-html-headline :override
              (lambda (headline contents info)
                "Transcode a HEADLINE element from Org to HTML.
CONTENTS holds the contents of the headline.  INFO is a plist
holding contextual information."
                (unless (org-element-property :footnote-section-p headline)
                  (let* ((numberedp (org-export-numbered-headline-p headline info))
                         (numbers (org-export-get-headline-number headline info))
                         (level (+ (org-export-get-relative-level headline info)
                                   (1- (plist-get info :html-toplevel-hlevel))))
                         (todo (and (plist-get info :with-todo-keywords)
                                    (let ((todo (org-element-property :todo-keyword headline)))
                                      (and todo (org-export-data todo info)))))
                         (todo-type (and todo (org-element-property :todo-type headline)))
                         (priority (and (plist-get info :with-priority)
                                        (org-element-property :priority headline)))
                         (text (org-export-data (org-element-property :title headline) info))
                         (tags (and (plist-get info :with-tags)
                                    (org-export-get-tags headline info)))
                         (full-text (funcall (plist-get info :html-format-headline-function)
                                             todo todo-type priority text tags info))
                         (contents (or contents ""))
                         (id (org-html--reference headline info))
                         (formatted-text
                          (if (plist-get info :html-self-link-headlines)
                              (format "<a href=\"#%s\">%s</a>" id full-text)
                            full-text)))
                    (if (org-export-low-level-p headline info)
                        ;; This is a deep sub-tree: export it as a list item.
                        (let* ((html-type (if numberedp "ol" "ul")))
                          (concat
                           (and (org-export-first-sibling-p headline info)
                                (apply #'format "<%s class=\"org-%s\">\n"
                                       (make-list 2 html-type)))
                           (org-html-format-list-item
                            contents (if numberedp 'ordered 'unordered)
                            nil info nil
                            (concat (org-html--anchor id nil nil info) formatted-text)) "\n"
                           (and (org-export-last-sibling-p headline info)
                                (format "</%s>\n" html-type))))
                      ;; Standard headline.  Export it as a section.
                      (let ((extra-class
                             (org-element-property :HTML_CONTAINER_CLASS headline))
                            (headline-class
                             (org-element-property :HTML_HEADLINE_CLASS headline))
                            (first-content (car (org-element-contents headline))))
                        (format "<%s id=\"%s\" class=\"%s\">%s%s</%s>\n"
                                (org-html--container headline info)
                                (format "outline-container-%s" id)
				                (concat (format "outline-%d" level)
					                    (and extra-class " ")
					                    extra-class)
				                (format "\n<h%d id=\"%s\"%s>%s</h%d>\n"
					                    (- level 1)
					                    id
					                    (if (not headline-class) ""
					                      (format " class=\"%s\"" headline-class))
					                    (concat
					                     (and numberedp
					                          (format
					                           "<span class=\"section-number-%d\">%s</span> "
					                           level
					                           (concat (mapconcat #'number-to-string numbers ".") ".")))
					                     formatted-text)
					                    (- level 1))
				                ;; When there is no section, pretend there is an
				                ;; empty one to get the correct <div
				                ;; class="outline-...> which is needed by
				                ;; `org-info.js'.
				                (if (eq (org-element-type first-content) 'section) contents
				                  (concat (org-html-section first-content "" info) contents))
				                (org-html--container headline info)))))))))

(use-package org-fragtog
  :hook
  (org-mode . org-fragtog-mode))

(use-package cdlatex)
