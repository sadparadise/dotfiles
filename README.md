# dotfiles

If you want to effortlessly setup a system with this config from zero on Arch Linux, you can use the scripts found in [this repo of mine](https://gitlab.com/akamofu/myarchinstall).
